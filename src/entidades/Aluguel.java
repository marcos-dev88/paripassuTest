package entidades;

import java.util.Date;
import java.util.List;

public class Aluguel {
    private Integer idAluguel;
    private List<Filme> filmes;
    private Cliente cliente;
    private Date dataAluguel;
    private float valor;

    public Aluguel() {
    }

    public Aluguel(Integer idAluguel, List<Filme> filmes, Cliente cliente, Date dataAluguel, float valor) {
        this.idAluguel = idAluguel;
        this.filmes = filmes;
        this.cliente = cliente;
        this.dataAluguel = dataAluguel;
        this.valor = valor;
    }

    public Integer getIdAluguel() {
        return idAluguel;
    }

    public void setIdAluguel(Integer idAluguel) {
        this.idAluguel = idAluguel;
    }

    public List<Filme> getFilmes() {
        return filmes;
    }

    public void setFilmes(List<Filme> filmes) {
        this.filmes = filmes;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Date getDataAluguel() {
        return dataAluguel;
    }

    public void setDataAluguel(Date dataAluguel) {
        this.dataAluguel = dataAluguel;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "Aluguel{" +
                "idAluguel=" + idAluguel +
                ", filmes=" + filmes +
                ", cliente=" + cliente +
                ", dataAluguel=" + dataAluguel +
                ", valor=" + valor +
                '}';
    }
}