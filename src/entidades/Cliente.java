package entidades;

import java.sql.Date;

public class Cliente {


    private Integer idCliente;
    private String nome;
    private Integer alugueis;
    private float total_gasto;
    private Integer ano;
    public Cliente() {
    }

    public Cliente(String nome) {
        this.nome = nome;
    }

    public Cliente(Integer idCliente, String nome, Integer alugueis, float total_gasto, Integer ano) {
        this.idCliente = idCliente;
        this.nome = nome;
        this.alugueis = alugueis;
        this.total_gasto = total_gasto;
        this.ano = ano;
    }

    public Cliente(Integer idCliente, String nome) {
        this.idCliente = idCliente;
        this.nome = nome;
    }

    public Cliente(Integer idcliente, String nome, Integer alugueis) {
        this.idCliente = idcliente;
        this.nome = nome;
        this.alugueis = alugueis;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public Cliente setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public Cliente setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public Integer getAlugueis() {
        return alugueis;
    }

    public void setAlugueis(Integer alugueis) {
        this.alugueis = alugueis;
    }

    public float getTotal_gasto() {
        return total_gasto;
    }

    public void setTotal_gasto(float total_gasto) {
        this.total_gasto = total_gasto;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "idCliente=" + idCliente +
                ", nome='" + nome + '\'' +
                ", alugueis=" + alugueis +
                ", total_gasto=" + total_gasto +
                ", ano=" + ano +
                '}';
    }
}