package impl;

import com.sun.istack.internal.NotNull;
import dao.ClienteDAO;
import entidades.Cliente;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ClienteDAOImpl implements ClienteDAO {

    @Override
    public void insert(Connection conn, Cliente cliente) throws Exception {


        PreparedStatement myStmt = conn.prepareStatement("insert into en_cliente (id_cliente, nome) values (?, ?)");

        Integer idCliente = this.getNextId(conn);

        myStmt.setInt(1, idCliente);
        myStmt.setString(2, cliente.getNome());
        myStmt.execute();
        conn.commit();

        cliente.setIdCliente(idCliente);

    }

    @Override
    public void edit(Connection conn, Cliente cliente) throws Exception {

        PreparedStatement myStmt = conn.prepareStatement("update en_cliente set nome = (?) where id_cliente = (?)");

        myStmt.setString(1, cliente.getNome());
        myStmt.setInt(2, cliente.getIdCliente());

        myStmt.execute();
        conn.commit();
    }

    @Override
    public void delete(Connection conn, Integer idCliente) throws Exception {

        PreparedStatement myStmt = conn.prepareStatement("delete from en_cliente where id_cliente = ?");

        myStmt.setInt(1, idCliente);

        myStmt.execute();
        conn.commit();

    }

    @Override
    public Collection<Cliente> list(Connection conn, Integer tipoConsulta) throws Exception {

        final String sqlCode = "select * from en";


        PreparedStatement myStmt = conn.prepareStatement(sqlCode);
        ResultSet myRs = myStmt.executeQuery();

        Collection<Cliente> items = new ArrayList<>();

        while (myRs.next()) {
            Integer idCliente = myRs.getInt("id_cliente");
            String nome = myRs.getString("nome");
            Integer alugueis = myRs.getInt("alugueis");
            float total_gasto = myRs.getFloat("total_gasto");
            Integer data_aluguel = myRs.getInt("ano");

            items.add(new Cliente(idCliente, nome, alugueis, total_gasto, data_aluguel));
        }
        return null;
    }

    @Override
    public Cliente find(Connection conn, Integer idCliente) throws Exception {
        PreparedStatement myStmt = conn.prepareStatement("select * from en_cliente where id_cliente = ?");

        myStmt.setInt(1, idCliente);

        ResultSet myRs = myStmt.executeQuery();

        if (!myRs.next()) {
            return null;
        }

        String nome = myRs.getString("nome");
        return new Cliente(idCliente, nome);
    }

    @Override
    public Integer getNextId(Connection conn) throws Exception {
        PreparedStatement myStmt = conn.prepareStatement("select nextval('seq_en_cliente')");
        ResultSet rs = myStmt.executeQuery();
        rs.next();
        return rs.getInt(1);
    }

    @Override
    public List<Cliente> countAluguelC(Connection conn) throws Exception {
        final String sqlCode = "select en_cliente.id_cliente, en_cliente.nome, " +
                "count(en_aluguel.id_aluguel) as alugueis from en_cliente" +
                 " left join en_aluguel  on en_aluguel.id_cliente = en_cliente.id_cliente " +
                "group by en_cliente.id_cliente " +
                "order by alugueis asc";

        PreparedStatement myStmt = conn.prepareStatement(sqlCode);
        ResultSet resultSet = myStmt.executeQuery();
        List<Cliente> itensConsulta = new ArrayList<>();

            while(resultSet.next()) {
                Integer idcliente = resultSet.getInt("id_cliente");
                String nome = resultSet.getString("nome");
                Integer alugueis = resultSet.getInt("alugueis");
                itensConsulta.add(new Cliente(idcliente, nome, alugueis));
            }
        return itensConsulta;
    }

    @Override
    public List<Cliente> countTotalGasto(Connection conn) throws Exception {
        final String sqlCode = "select en_cliente.id_cliente, en_cliente.nome, " +
                "count(en_aluguel.id_cliente) as alugueis, " +
                "sum(en_aluguel.valor) as total_gasto, " +
                " extract (year from en_aluguel.data_aluguel) as ano " +
                " from en_cliente " +
                " inner join en_aluguel on en_aluguel.id_cliente = en_cliente.id_cliente " +
                " group by en_cliente.id_cliente, ano " +
                " order by total_gasto desc;";

        PreparedStatement myStmt = conn.prepareStatement(sqlCode);
        ResultSet resultSet = myStmt.executeQuery();
        List<Cliente> itensCliente = new ArrayList<>();

        while (resultSet.next()){
            Integer idCliente = resultSet.getInt("id_cliente");
            String nome = resultSet.getString("nome");
            Integer alugueis = resultSet.getInt("alugueis");
            float total_gasto = resultSet.getFloat("total_gasto");
            Integer data_aluguel = resultSet.getInt("ano");
            itensCliente.add(new Cliente(idCliente, nome, alugueis, total_gasto, data_aluguel));
        }
        return itensCliente;
    }


}
