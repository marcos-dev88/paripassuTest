package impl;

import dao.FilmeDAO;
import entidades.Filme;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

public class FilmeDAOImpl implements FilmeDAO {
    @Override
    public void insert(Connection conn, Filme filme) throws Exception {

            PreparedStatement myStmt = conn.prepareStatement("INSERT INTO en_filme (id_filme, dataLancamento, nome, descricao) VALUES ( ?, ?, ?, ?)");

            Integer idFilme = this.getNextId(conn);

            myStmt.setInt(1, idFilme);
            myStmt.setDate(2, (Date) filme.getDataLancamento());
            myStmt.setString(3, filme.getNome());
            myStmt.setString(4, filme.getDescricao());

            myStmt.execute();
            conn.commit();

            filme.setIdFilme(idFilme);
    }

    @Override
    public Integer getNextId(Connection conn) throws Exception {

            PreparedStatement myStmt = conn.prepareStatement("SELECT nextval('seq_en_filme')");

            ResultSet resultSet = myStmt.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);

    }

    @Override
    public void edit(Connection conn, Filme filme) throws Exception {

            PreparedStatement myStmt = conn.prepareStatement("UPDATE en_filme set dataLancamento = ?, nome = ?, descricao = ? WHERE id_filme = ?");

            myStmt.setDate(1, (Date)filme.getDataLancamento());
            myStmt.setString(2, filme.getNome());
            myStmt.setString(3, filme.getDescricao());
            myStmt.setInt(4, filme.getIdFilme());

            myStmt.execute();
            conn.commit();

    }

    @Override
    public void delete(Connection conn, Integer idFilme) throws Exception {

            PreparedStatement myStmt = conn.prepareStatement("DELETE FROM en_filme WHERE id_filme = ?");
            myStmt.setInt(1, idFilme);

            myStmt.execute();
            conn.commit();

    }

    @Override
    public Filme find(Connection conn, Integer idFilme) throws Exception {
        PreparedStatement myStmt = conn.prepareStatement("SELECT * FROM en_filme WHERE id_filme = ?");
        myStmt.setInt(1, idFilme);

        ResultSet resultSet = myStmt.executeQuery();
        if(!resultSet.next()) {
            return null;
        }

        Date dataLancamento = resultSet.getDate("data_lancamento");
        String nome = resultSet.getString("nome");
        String descricao = resultSet.getString("descricao");
        return new Filme(idFilme, dataLancamento, nome, descricao);
    }

    @Override
    public Collection<Filme> list(Connection conn) throws Exception {
        final String sqlCode = "SELECT  * FROM en_filme ORDER BY data_lancamento ASC";

        PreparedStatement myStmt = conn.prepareStatement(sqlCode);
        ResultSet resultSet = myStmt.executeQuery();

        Collection<Filme> itensFilmes = new ArrayList<>();

        while(resultSet.next()){

            Integer idFilme = resultSet.getInt("id_filme");
            Date dataLancamento = resultSet.getDate("data_lancamento");
            String nome = resultSet.getString("nome");
            String descricao = resultSet.getString("descricao");
            itensFilmes.add(new Filme(idFilme, dataLancamento, nome, descricao));

        }
        return itensFilmes;
    }

    @Override
    public Filme findDataLanc(Connection conn) throws Exception {
        final String sqlCode = "select * from en_filme GROUP BY en_filme.id_filme ORDER BY MIN(data_lancamento) ASC;";

        PreparedStatement myStmt = conn.prepareStatement(sqlCode);

        ResultSet resultSet = myStmt.executeQuery();

        while(resultSet.next()){
            Integer idFilme = resultSet.getInt("id_filme");
            Date dataLancamento = resultSet.getDate("data_lancamento");
            String nome = resultSet.getString("nome");
            String descricao = resultSet.getString("descricao");
            return new Filme(idFilme, dataLancamento, nome, descricao);
        }
        return null;
    }
}
