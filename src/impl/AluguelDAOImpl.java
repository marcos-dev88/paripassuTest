package impl;

import dao.AluguelDAO;
import entidades.Aluguel;
import entidades.Cliente;
import entidades.Filme;

import java.sql.*;
import java.sql.Date;
import java.util.*;

public class AluguelDAOImpl implements AluguelDAO {

    @Override
    public void insert(Connection conn, Aluguel aluguel) throws Exception {

            String sqlCode1 = "INSERT INTO en_aluguel(id_aluguel, id_cliente, data_aluguel, valor) VALUES (?, ?, ?, ?)";
            StringBuilder sqlCode2 = new StringBuilder("INSERT INTO re_aluguel_filme(id_aluguel, id_filme) VALUES ");

            PreparedStatement myStmt = conn.prepareStatement(sqlCode1);
            aluguel.setIdAluguel(this.getNextId(conn));

            for(int i = 0; i < aluguel.getFilmes().size(); i++){
                Filme f = aluguel.getFilmes().get(i);
                sqlCode2.append("(").append(aluguel.getIdAluguel()).append(",").append(f.getIdFilme()).append(")");
                if(aluguel.getFilmes().size() > 1 && i < aluguel.getFilmes().size()){
                    sqlCode2.append(",");
                }else if( i >= aluguel.getFilmes().size()) {
                    sqlCode2.append(";");
                }
            }
        PreparedStatement myStmt2 = conn.prepareStatement(sqlCode2.toString());

            myStmt.setInt(1, aluguel.getIdAluguel());
            myStmt.setInt(2, aluguel.getCliente().getIdCliente());
            myStmt.setDate(3, (Date) aluguel.getDataAluguel());
            myStmt.setFloat(4, aluguel.getValor());

            myStmt.executeUpdate();
            myStmt2.executeUpdate();
            conn.commit();

    }

    @Override
    public Integer getNextId(Connection conn) throws Exception {
        PreparedStatement myStmt = conn.prepareStatement("SELECT nextval('seq_en_aluguel')");
        ResultSet rs = myStmt.executeQuery();
        rs.next();
        return rs.getInt(1);
    }

    @Override
    public void edit(Connection conn, Aluguel aluguel) throws Exception {
        try {
            PreparedStatement myStmt = conn.prepareStatement("UPDATE en_aluguel set id_cliente = ?, dataAluguel = ?, valor = ? WHERE id_aluguel = ?");

            myStmt.setObject(1, aluguel.getCliente());
            myStmt.setDate(2, (Date) aluguel.getDataAluguel());
            myStmt.setFloat(3, aluguel.getValor());

            myStmt.execute();
            conn.commit();
        } catch (Exception e) {
            System.out.println("Erro ao dar update em Aluguel: " + e);
        }
    }

    @Override
    public void delete(Connection conn, Aluguel aluguel) throws Exception {
        try {
            String sqlCode = " DELETE FROM re_aluguel_filme WHERE id_aluguel = ?; "+
                    " DELETE FROM en_aluguel WHERE id_aluguel = ?";
            PreparedStatement myStmt = conn.prepareStatement(sqlCode);

            myStmt.setInt(1, aluguel.getIdAluguel());

            myStmt.execute();
            conn.commit();
        } catch (Exception e) {
            System.out.println("Erro ao deletar Aluguel: " + e);
        }
    }

    @Override
    public Aluguel find(Connection conn, Integer idAluguel) throws Exception {

        final String codSQL = "select en_cliente.nome, en_aluguel.*, " +
                " count(re_aluguel_filme.id_filme) as filmes " +
                " from re_aluguel_filme " +
                " inner join en_aluguel on en_aluguel.id_aluguel = re_aluguel_filme.id_aluguel " +
                " inner join en_cliente on en_cliente.id_cliente = en_aluguel.id_cliente  WHERE re_aluguel_filme.id_aluguel = ? " +
                " group by en_aluguel.id_aluguel, en_cliente.id_cliente " +
                " order by id_aluguel ASC;";


            PreparedStatement preparedStmt = conn.prepareStatement(codSQL);
            preparedStmt.setInt(1, idAluguel);
            ResultSet resultSet = preparedStmt.executeQuery();

            if (!resultSet.next()) {
               return null;
            }

            Cliente c = new Cliente();

            Integer idcliente =  resultSet.getInt("id_cliente");
            c.setIdCliente(idcliente);
            Integer filmes = resultSet.getInt("filmes");
            Date dataAluguel = resultSet.getDate("data_aluguel");
            float valor = resultSet.getFloat("valor");

        return new Aluguel(idAluguel, getFilmesR(conn, idAluguel), c, dataAluguel, valor);
    }

    @Override
    public List<Aluguel> list(Connection conn) throws Exception {


        final String sqlCode = "select en_aluguel.*, " +
                "count(re_aluguel_filme.id_filme) as filmes " +
                "from re_aluguel_filme " +
                "inner join en_aluguel on en_aluguel.id_aluguel = re_aluguel_filme.id_aluguel " +
                "inner join en_cliente on en_cliente.id_cliente = en_aluguel.id_cliente "+
                "group by en_aluguel.id_aluguel " +
                "order by id_aluguel ASC";

        PreparedStatement myStmt = conn.prepareStatement(sqlCode);
        ResultSet resultSet = myStmt.executeQuery();

        List<Aluguel> itensAluguel = new ArrayList<>();

        while(resultSet.next()){
            Integer idAluguel = resultSet.getInt("id_aluguel");
            Cliente c = new Cliente();
            Integer idCliente = resultSet.getInt("id_cliente");
            c.setIdCliente(idCliente);
            Date dataAluguel = resultSet.getDate("data_aluguel");
            float valor = resultSet.getFloat("valor");
            itensAluguel.add(new Aluguel(idAluguel, getFilmesR(conn, idAluguel), c, dataAluguel, valor));
        }
        return itensAluguel;
    }

    private List<Filme> getFilmesR(Connection conn, Integer idAluguel){

        try {
            final String codSQL = "SELECT en_filme.* from re_aluguel_filme" +
                    " INNER JOIN en_filme on re_aluguel_filme.id_filme = en_filme.id_filme " +
                    " WHERE re_aluguel_filme.id_aluguel = ?";

            PreparedStatement myStmt = conn.prepareStatement(codSQL);
            myStmt.setInt(1, idAluguel);
            ResultSet resultSet = myStmt.executeQuery();
            List<Filme> filmes = new ArrayList<>();

            while(resultSet.next()){
              Integer idFilme = resultSet.getInt("id_filme");
              Date data_lancamento = resultSet.getDate("data_lancamento");
              String nome = resultSet.getString("nome");
              String descricao = resultSet.getString("descricao");

              filmes.add(new Filme(idFilme, data_lancamento, nome, descricao));
            }

            return filmes;
        }catch (SQLException e){
            System.out.println("Erro na lista de filmes: "+e);
        }
        return null;
    }
}