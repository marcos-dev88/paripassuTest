package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {

    public Connection ConexaoBanco() throws Exception {
        try {
            Connection conn = null;
            conn = (Connection) DriverManager.getConnection("jdbc:postgresql://127.0.0.1:8090/paripassutest", "postgres", "Info@1234");
            Class.forName("org.postgresql.Driver");
            conn.setAutoCommit(false);
            return conn;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }
}
