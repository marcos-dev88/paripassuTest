package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class FormatadorDate {

    public java.sql.Date formatarData(String date) throws Exception {
        try {
            SimpleDateFormat valorString = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            java.sql.Date sqlDate = new java.sql.Date(valorString.parse(date).getTime());
            return sqlDate;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
