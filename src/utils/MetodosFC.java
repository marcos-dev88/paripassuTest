package utils;

import dao.AluguelDAO;
import dao.ClienteDAO;
import entidades.Aluguel;
import entidades.Cliente;
import impl.AluguelDAOImpl;
import impl.ClienteDAOImpl;

import java.util.List;

public class MetodosFC {

    public List<Aluguel> listarAlugueis() throws Exception{
        Conexao conn = new Conexao();
        AluguelDAO aluguelDAO = new AluguelDAOImpl();
        List<Aluguel> alugueis = aluguelDAO.list(conn.ConexaoBanco());
        for(Aluguel aluguel : alugueis){
            System.out.println("|Id_Aluguel: "+String.format("%-5s |", aluguel.getIdAluguel())
                    +" Id_Cliente: "+String.format("%-4s |", aluguel.getCliente().getIdCliente())
                    +" Data_Aluguel: "+String.format("%.10s |", aluguel.getDataAluguel())
                    +" Valor: "+String.format("%-6s |" ,aluguel.getValor()));
        }
        return alugueis;
    }

    public List<Cliente> listarClientesAluguel() throws Exception {
        Conexao conn = new Conexao();
        ClienteDAO clienteDAO = new ClienteDAOImpl();
        List<Cliente> clientes = clienteDAO.countAluguelC(conn.ConexaoBanco());
        for (Cliente itens : clientes) {
            if (itens.getNome() != null && itens.getIdCliente() != null && itens.getAlugueis() != null) {
                System.out.println("|ID: " + String.format("%-3s |", itens.getIdCliente()) + "Nome: " + String.format("%-10s |", itens.getNome()) + "Alugueis: " + String.format("%-3s |", itens.getAlugueis()));
            }
        }
        return clientes;
    }

    public List<Cliente> listarTotalGastoAluguel() throws Exception{
        Conexao conn = new Conexao();
        ClienteDAO clienteDAO = new ClienteDAOImpl();
        List<Cliente> clientesAluguel = clienteDAO.countTotalGasto(conn.ConexaoBanco());
        for (Cliente itens : clientesAluguel){
            if(itens.getIdCliente() != null && itens.getNome() != null && itens.getAlugueis() != null && itens.getTotal_gasto() != 0 && itens.getAno() != 0){
                System.out.println("|ID: "+String.format("%-3s |",itens.getIdCliente())+"Nome: "+String.format("%-10s |",itens.getNome())+
                        "Alugueis: "+itens.getAlugueis()+" |Total Gasto: "+String.format("%-9s |", itens.getTotal_gasto())+"Ano: "+itens.getAno()+" |");
            }
        }
        return clientesAluguel;
    }

    public void findAluguel(int idAluguel) throws Exception{
        Conexao conn = new Conexao();
        AluguelDAO aluguelDAO = new AluguelDAOImpl();
        if(aluguelDAO.find(conn.ConexaoBanco(), idAluguel) != null) {
            System.out.println(aluguelDAO.find(conn.ConexaoBanco(), idAluguel));
        }else{
            System.out.println("Não foi encontrado um aluguel com este id no banco.");
        }
    }
}
