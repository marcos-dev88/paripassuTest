import dao.AluguelDAO;
import dao.ClienteDAO;
import dao.FilmeDAO;
import entidades.Aluguel;
import entidades.Cliente;
import entidades.Filme;
import utils.Conexao;
import utils.FormatadorDate;
import impl.AluguelDAOImpl;
import impl.ClienteDAOImpl;
import impl.FilmeDAOImpl;
import utils.MetodosFC;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class Main {

    public static void main(String[] args) throws Exception {

        Conexao conn = new Conexao();
        MetodosFC mFc = new MetodosFC();
        FormatadorDate formatadorDate = new FormatadorDate();

        //Demonstrar o funcionamento aqui
        ClienteDAO clienteDAO = new ClienteDAOImpl();
        FilmeDAO filmeDAO = new FilmeDAOImpl();
        AluguelDAO aluguelDAO = new AluguelDAOImpl();

        Date dataAluguel = formatadorDate.formatarData("2020-09-11"); //Coloque aqui a data que desejas, passe a variável onde desejares
        //imprimir essa data ;)

        mFc.findAluguel(1); //Coloque o id do aluguel que desejas procurar aqui

        //Inserir dados no banco:
        //Filmes
        List<Filme> filmes = new ArrayList<>();
        filmes.add(new Filme(2));

        //Cliente:
        Cliente c = new Cliente("Reginaldo");
        clienteDAO.insert(conn.ConexaoBanco(), c);

        //Aluguel:
        Aluguel aluguel1 = new Aluguel(7, filmes,c, dataAluguel, 9.8f);
        aluguelDAO.insert(conn.ConexaoBanco(),aluguel1);

        System.out.println("");
        System.out.println("-------------------------------------------------------------------------------");
        mFc.listarAlugueis();
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("");
        System.out.println("");

        //Teste SQL:

        //=================== Desafio 1 SQL ===================================//
        System.out.println(filmeDAO.findDataLanc(conn.ConexaoBanco()));
        System.out.println("");
        //=================== Desafio 2 SQL ===================================//
        System.out.println("------------------------------------------");
        mFc.listarClientesAluguel();
        System.out.println("------------------------------------------");
        System.out.println("/////////////////////////////////////////");//Espaços
        //=================== Desafio 3 SQL ===================================//
        System.out.println("---------------------------------------------------------------------------");
        mFc.listarTotalGastoAluguel();
        System.out.println("---------------------------------------------------------------------------");

        System.out.println("Fim do teste.");
    }
}