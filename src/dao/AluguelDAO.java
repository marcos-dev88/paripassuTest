package dao;

import entidades.Aluguel;
import entidades.Filme;

import java.sql.Connection;
import java.sql.Date;
import java.util.Collection;
import java.util.List;

public interface AluguelDAO {

    void insert(Connection conn, Aluguel aluguel) throws Exception;

    Integer getNextId(Connection conn) throws Exception;

    void edit(Connection conn, Aluguel aluguel) throws Exception;

    void delete(Connection conn, Aluguel aluguel) throws Exception;

    Aluguel find(Connection conn, Integer idAluguel) throws Exception;

   List<Aluguel> list(Connection conn) throws Exception;
}